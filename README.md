Jira Ticket Creator
===================

Installation
------------

1. `git clone`
2. `(sudo) python setup.py install`

Problems installing?
--------------------

Reportlab has a lot of dependencies, some of which require specific libraries to be installed into the system.
Here're common problems and solutions:

Installation fails on pillow
============================

Pillow needs one of graphical libraries: libjpeg, libpng in the system

on mac you can install it with

```
brew install libjpeg
```

xcrum, clang failed
===================

Pillow needs to be compiled, so it needs a c compiler in the system

on macos:

```
xcode-select --install
```

Usage
-----
```
  print_tickets -h
```

When first used it will ask for:

 * jira server (type your jira server)
 * jira username
 * jira password

all this information is stored locally in binary db
