# -*- coding: utf-8 -*-

from cgi import escape
# Third party modules
from reportlab.lib.pagesizes import A4
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.platypus import SimpleDocTemplate, Paragraph
from reportlab.platypus.flowables import KeepTogether

# Own
from templates.item.base import STYLES, BaseItemTemplate

SUMMARY_LIMIT = 140


class PdfCreator(object):
    def __init__(self, tickets, output):
        self.tickets = tickets
        self.make_pdf(output)

    def make_pdf(self, output):
        doc = SimpleDocTemplate(output, pagesize=A4)

        elements = [KeepTogether(self.__get_ticket_template(ticket))
                    for ticket in self.tickets]
        doc.build(elements)

    def sanitize_text(self, txt):
      return unicode(txt).encode('ascii', 'replace')

    def __get_ticket_template(self, ticket):
        issuetype = ticket.fields.issuetype.name.upper()

        if issuetype in STYLES:
            theme = STYLES[issuetype]
        else:
            theme = STYLES['DEFAULT']

        styles = getSampleStyleSheet()
        style = styles.get('Normal')
        style.fontSize = 30
        style.fontName = 'Helvetica-Bold'
        style.leading = 34

        main_content = ticket.fields.summary

        if len(main_content) > SUMMARY_LIMIT:
            main_content = main_content[:SUMMARY_LIMIT - 5] + '(...)'

        main_content = escape(main_content)

        return BaseItemTemplate({
            'top_left': self.sanitize_text(ticket.key),
            'top_right': self.sanitize_text(ticket.fields.creator) if hasattr(ticket.fields, 'creator') else self.sanitize_text(ticket.fields.reporter),  # noqa
            'main_content': Paragraph(self.sanitize_text(main_content), style),
        }, style=theme).get()
