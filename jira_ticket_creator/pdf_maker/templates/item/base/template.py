# -*- coding: utf-8 -*-

# Python Standard Library
from abc import ABCMeta, abstractmethod

# Third party modules
from reportlab.platypus import Table, TableStyle

# Local imports
from .theme import SIZE, STYLES


class AbstractBaseItemTemplate:
    __metaclass__ = ABCMeta

    """Single ticket abstraction for pdfcreator

    Abstraction to allow decoupling the pdf page in easier to manage
    subtemplates, also should allow to swap the templates more easily.

    Args:
        data (dict of str: str): data to populate template with, should have
        `top_left`, `top_middle`, `top_right` and `main_content` keys.
        style (Optional[reportlab.platypus.TableStyle]): style of table
        size (Optional[dict of str: list]): list of field sizes

    Raises:
        TypeError: if arguments are not appropriate types
        KeyError: if `size` does not have appropriate keys (`rows`, `columns`)
    """

    def __init__(self, data, style=STYLES['DEFAULT'], size=SIZE):

        if not type(data) is dict:
            raise TypeError('''data argument needs to be dict, got %s''' %
                            data.__class__.__name__)

        if not isinstance(style, TableStyle):
            raise TypeError('''style argument needs to be instance of TableStyle,
                            got %s''' % style.__class__.__name__)

        if not type(size) is dict:
            raise TypeError('''size argument needs to be dict, got %s''' %
                            size.__class__.__name__)

        if 'rows' not in size or 'columns' not in size:
            raise KeyError('''size argument needs to be dict with `rows`
                           and `columns` keys''')

        self.__data = self._clean_data(data)
        self.__style = style
        self.__size = size

    @abstractmethod
    def _clean_data(self, data):
        """method to prepare data

        Args:
            data (dict of str: str): data to clean

        Returns:
            dict: with keys `top_left`, `top_middle`, `top_right`,
                `main_content`
        """
        pass

    def get(self):
        """method to return Table

        Returns:
            reportlab.platypus.Table: table
        """

        data = [
            [self.__data['top_left'], self.__data['top_middle'],
             self.__data['top_right']],
            [self.__data['main_content'], '']
        ]

        table = Table(data, colWidths=self.__size['columns'],
                      rowHeights=self.__size['rows'])

        table.setStyle(self.__style)

        return table


class BaseItemTemplate(AbstractBaseItemTemplate):

    def _clean_data(self, data):
        return {
            'top_left': data.get('top_left', ''),
            'top_middle': data.get('top_middle', ''),
            'top_right': data.get('top_right', ''),
            'main_content': data.get('main_content', ''),
        }
