# -*- coding: utf-8 -*-

# Third party modules
from reportlab.lib import colors
from reportlab.lib.units import cm
from reportlab.platypus import TableStyle

_BASE = [
    ('ALIGN', (0, 0), (0, 0), 'CENTER'),
    ('ALIGN', (1, 0), (1, 0), 'LEFT'),
    ('ALIGN', (2, 0), (2, 0), 'RIGHT'),
    ('VALIGN', (1, 0), (1, 0), 'MIDDLE'),
    ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'),
    ('BOX', (0, 0), (-1, -1), 1, colors.black),
    ('BOX', (0, 0), (0, 0), 1, colors.black),
    ('FONT', (0, 0), (-1, -1), 'Helvetica-Bold'),
    ('LEADING', (0, 0), (-1, -1), 30),
    ('LEFTPADDING', (0, 1), (0, 1), 20),
    ('LINEBELOW', (0, 0), (-1, 0), 1, colors.black),
    ('LINERIGHT', (0, 0), (0, 0), 1, colors.black),
    ('RIGHTPADDING', (0, 1), (0, 1), 20),
    ('SIZE', (0, 0), (-1, -1), 26),
    ('SIZE', (0, 0), (0, 0), 28),
    ('SIZE', (1, 0), (1, 0), 18),
    ('SIZE', (2, 0), (2, 0), 20),
    ('SPAN', (0, 1), (2, 1)),
    ('TEXTCOLOR', (1, 0), (1, 0), colors.cornflower),
]

_STORY = _BASE + [
    ('BACKGROUND', (0, 0), (0, 0), colors.green),
    ('TEXTCOLOR', (0, 0), (0, 0), colors.white),
]

_BUG = _BASE + [
    ('BACKGROUND', (0, 0), (0, 0), colors.red),
    ('TEXTCOLOR', (0, 0), (0, 0), colors.white),
]

_INVESTIGATION = _BASE + [
    ('BACKGROUND', (0, 0), (0, 0), colors.yellow),
    ('TEXTCOLOR', (0, 0), (0, 0), colors.black),
]

_TASK = _BASE + [
    ('BACKGROUND', (0, 0), (0, 0), colors.HexColor('#bfe4ff')),
    ('TEXTCOLOR', (0, 0), (0, 0), colors.black),
]

_DEFAULT = _STORY

_ALL_STYLES = ['STORY', 'BUG', 'INVESTIGATION', 'TASK', 'DEFAULT']

STYLES = {}

for style in _ALL_STYLES:
    STYLES[style] = TableStyle(globals()['_%s' % style])

SIZE = {
    'columns': [8 * cm, 6 * cm,  5 * cm],
    'rows': [1.6 * cm, 6.4 * cm],
}
