# -*- coding: utf-8 -*-

# Python Standard Library
import os
import subprocess
import sys
from tempfile import gettempdir, gettempprefix
import argparse

# Own
from pdf_maker import PdfCreator
from query_builder import Ticket

FILTERS = ['priority', 'type', 'sprint', 'project', 'status', 'creator',
           'component', 'resolution']


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--output', help='Where to put pdf file', type=str)
    parser.add_argument('--server', help='Force to use specific JIRA instance', type=str)
    parser.add_argument('--nosubtickets', help='Do not include subtickets', type=bool)
    parser.add_argument('ids', help='filter by ticket keys', nargs="*",
                        type=str)

    for filter in FILTERS:
        parser.add_argument('--%s' % filter, nargs="*",
                            help='filter by %s' % filter, type=str)

    parsed, _ = parser.parse_known_args()

    tickets = Ticket(forcedserver=getattr(parsed, 'server'), nosubtickets=getattr(parsed, 'nosubtickets', False))

    for filter in FILTERS:
        if getattr(parsed, filter):
            tickets = getattr(tickets, 'by_%s' % filter)(getattr(parsed,
                                                                 filter))

    if getattr(parsed, 'ids'):
        tickets = tickets.by_id(parsed.ids)

    if getattr(parsed, 'output'):
        output = parsed.output
    else:
        output = os.path.join(gettempdir(), '%s-printt.pdf' % gettempprefix())

    PdfCreator(tickets=tickets(), output=output)

    if sys.platform.startswith('darwin'):  # MACOS
        subprocess.call(('open', output))
    elif os.name == 'nt':  # Windows
        os.startfile(output)
    elif os.name == 'posix':  # Unix/Linux
        subprocess.call(('xdg-open', output))
