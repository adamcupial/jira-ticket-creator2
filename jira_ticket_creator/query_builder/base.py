# -*- coding: utf-8 -*-

# Python Standard Library
import functools
from abc import ABCMeta

# Third party modules
import jira

# Own
from jira_ticket_creator.settings import PersistantConfig


class AbstractQuery:
    __metaclass__ = ABCMeta

    FILTERS = {}

    def __init__(self, forcedserver=None, nosubtickets=False):
        self.__query = {}
        self.nosubtickets = nosubtickets
        config = PersistantConfig()
        server, username, password = config.jira_credentials

        if forcedserver is not None:
            server = forcedserver

        self.jira = jira.client.JIRA(options={'server': server},
                                     basic_auth=(username, password))

    def __str__(self):
        query_list = []

        for key, value in self.__query.items():
            if hasattr(value, '__iter__'):
                value = list(set([x for x in value]))
                query = '%s in (%s)' % (key, ','.join(
                    ['"%s"' % x for x in value]))
            elif key.endswith('*'):
                query = '%s ~ %s' % (key[:-1], value)
            else:
                query = '%s = %s' % (key, value)

            query_list.append(query)

            stringquery = ' AND '.join(query_list)

        if self.nosubtickets:
           stringquery += ' AND issuetype in standardIssueTypes() '

        return stringquery

    def __getattr__(self, name):
        if name.startswith('by_') and name[3:] in \
           self.FILTERS.keys():
            return functools.partial(self._filter_by,
                                     self.FILTERS[name[3:]])
        return getattr(self, name)

    def __call__(self):
        print('Searching with query: "%s"' % str(self))
        issues = self.jira.search_issues(str(self), maxResults=1000)

        return issues

    def __repr__(self):
        return '%s with query: "%s"' % (self.__class__.__name__, str(self))

    def _filter_by(self, field, values, fuzzy=False):
        if fuzzy:
            self.__query['%s*' % field] = values
        else:
            self.__query[field] = values

        return self
