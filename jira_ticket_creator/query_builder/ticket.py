# -*- coding: utf-8 -*-

from .base import AbstractQuery


class Ticket(AbstractQuery):
    FILTERS = {
        'project': 'project',
        'id': 'id',
        'sprint': 'sprint',
        'type': 'type',
        'priority': 'priority',
        'status': 'status',
        'creator': 'creator',
        'component': 'component',
        'resolution': 'resolution',
    }

    def by_summary(self, keyword):
        return self._filter_by('summary', keyword, fuzzy=True)

    def by_description(self, keyword):
        return self._filter_by('description', keyword, fuzzy=True)
