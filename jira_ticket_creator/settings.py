# -*- coding: utf-8 -*-

# Python Standard Library
import getpass
import os
import shelve

# Third party modules
import jira


class PersistantConfig(object):

    def __init__(self):
        home = os.path.expanduser('~')
        dir = os.path.join(home, '.config', 'jira-ticket-creator')
        if not os.path.isdir(dir):
            os.makedirs(dir)
        filename = os.path.join(dir, 'config.anydb')
        self.storage = shelve.open(filename)

    def __getitem__(self, key):
        if hasattr(self, key):
            return getattr(self, key)
        else:
            return self.storage[key]

    def __setitem__(self, key, value):
        if hasattr(self, key):
            setattr(self, key, value)
        else:
            self.storage[key] = value
            self.storage.sync()

    @property
    def jira_credentials(self):
        try:
            server = self['server']
            username = self['username']
            password = self['password']
        except KeyError:
            server, username, password = self.get_and_validate_credentials()
            self.jira_credentials = server, username, password

        return server, username, password

    def get_and_validate_credentials(self):
        server = raw_input('jira server: ')
        username = raw_input('jira user: ')
        password = getpass.getpass('jira password: ')

        if not server.startswith('http'):
            server = 'https://%s' % server

        try:
            conn = jira.client.JIRA(options={'server': server},
                                    basic_auth=(username, password))
            conn.projects()
        except Exception as e:
            print('Exception "%s" is raised: "%s", please try again' %
                  (e.__class__.__name__, e.message))
            return self.get_and_validate_credentials()

        return server, username, password

    @jira_credentials.setter
    def jira_credentials(self, value):
        self['server'], self['username'], self['password'] = value
