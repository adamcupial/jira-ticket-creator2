#!/usr/bin/env python

from setuptools import setup, find_packages

setup(
    name='TicketCreator',
    version='1.2',
    description='Goal Board Ticket Helper',
    author='Adam C',
    author_email='adam.cupial@performgroup.com',
    url='n/a',
    packages=find_packages(),
    install_requires=[
        'jira>=1.0.7.dev',
        'reportlab',
    ],
    entry_points={
        'console_scripts': [
            'print_tickets = jira_ticket_creator.print_tickets:main',
        ]
    }
)
